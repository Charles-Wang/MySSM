package com.yh.bean;

public class Student {

	private int stu_Id;
	private String stu_Name;
	private String stu_Sex;
	private String stu_Age;
	public int getStu_Id() {
		return stu_Id;
	}
	public void setStu_Id(int stu_Id) {
		this.stu_Id = stu_Id;
	}
	public String getStu_Name() {
		return stu_Name;
	}
	public void setStu_Name(String stu_Name) {
		this.stu_Name = stu_Name;
	}
	public String getStu_Sex() {
		return stu_Sex;
	}
	public void setStu_Sex(String stu_Sex) {
		this.stu_Sex = stu_Sex;
	}
	public String getStu_Age() {
		return stu_Age;
	}
	public void setStu_Age(String stu_Age) {
		this.stu_Age = stu_Age;
	}
	
	
}
