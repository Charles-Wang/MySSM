package com.yh.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.yh.bean.Student;

public interface StudentDao {
	
	@Select("select * from student")
	public List<Student> queryStu();

}
