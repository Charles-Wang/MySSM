package com.yh.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.yh.bean.Student;
import com.yh.service.impl.StudentServiceImpl;

public class Test2 {

	@Test
	public void test() {
		
		ApplicationContext app = new ClassPathXmlApplicationContext("MySSM-servlet.xml");
		
		StudentServiceImpl studentServiceImpl = (StudentServiceImpl) app.getBean("studentServiceImpl");
		
		List<Student> list = studentServiceImpl.queryStu();
		
		System.out.println(list.size());
	}

}
