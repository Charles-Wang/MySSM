package com.yh.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yh.bean.Student;
import com.yh.dao.StudentDao;

@Component
public class StudentDaoImpl implements StudentDao {

	@Autowired
	private SqlSessionFactory sqlSessionFactory;
	@Override
	public List<Student> queryStu() {
		// TODO Auto-generated method stub
		SqlSession sqlSession = sqlSessionFactory.openSession();
		StudentDao stuDao = sqlSession.getMapper(StudentDao.class);
		List<Student> list = stuDao.queryStu();
		sqlSession.close();
		return list;
	}
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}
	
	

}
